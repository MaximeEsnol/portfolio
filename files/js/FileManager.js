/**
 * For opening files locally and remotely :)
 */
function open(url, method = "GET", data = null){
      return new Promise(function(resolve, reject){

            let xhr = new XMLHttpRequest();
            xhr.open(method, url);

            xhr.onload = function(){
                  if(this.status == 200 && this.readyState == 4){
                        resolve(xhr.response);
                  } else {
                        reject({
                              status: xhr.status,
                              statusText: xhr.statusText
                        });
                  }
            }

            if(data != null){
                  xhr.send(data);
            } else {
                  xhr.send();
            }
      });
      
}

function openJSON(url, method = "GET", data = null){
      return new Promise(function(resolve, reject){
            open(url, method, data)
            .then(function(e){
                  resolve(JSON.parse(e));
            })
            .catch(function(e){
                  reject(function(){
                        console.log(e.statusText);
                  });
            });
      });
}

/*
*     Calls a function to load content into an element. It will
*     mark the element as already loaded afterwards, so contents 
*     don't get loaded multiple times.
*/    
function loadIntoElement(element, callback){
      if(element.getAttribute('data-loaded') == "false"){
            element.setAttribute('data-loaded', 'true');
            callback().then(function(e){
                  console.log(e);
                  element.innerHTML = e;
            });
      } 
}