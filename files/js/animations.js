
/*
* Opens or closes the menu 
*/
function toggleMenu(){
      let menu = document.getElementById('menu');
      let menuToggle = document.getElementById('menu_icon').getElementsByTagName('i')[0];
      let state = menuToggle.getAttribute('data-opened');

      if(state == "false"){
            menuOpen(menu, menuToggle);
      } else {
            menuClose(menu, menuToggle);
      }
}

/*
* What happens when the menu opens 
*/
function menuOpen(menu, menuToggle){
      let displayBlock = function(){
            menu.style.display = "block";
            menu.style.height = "40vh";
            menuToggle.setAttribute('class', 'lni-close');
      };

      menuToggle.setAttribute('data-opened', 'true');
      menu.style.display = "block";
      menu.style.animation = 'menuOpen ease-in-out .5s';

      menuToggle.style.animation = 'iconChange ease-in-out 1s';     
      
      openMenuItems(menu);

      setTimeout(displayBlock, 450);

      setTimeout(function(){
            menuToggle.style.animation = 'none';   
      }, 1000);
}

/*
* What happens when the menu closes
*/
function menuClose(menu, menuToggle){
      let displayNone = function(){
            menu.style.display = "none";
            menu.style.height = "0vh";
            menuToggle.setAttribute('class', 'lni-menu');
      };
      
      menuToggle.setAttribute('data-opened', 'false');
      menu.style.animation = 'menuClose ease-in-out .5s';

      menuToggle.style.animation = 'iconChange ease-in-out 1s';  

      closeMenuItems(menu);

      setTimeout(displayNone, 450);

      setTimeout(function(){
            menuToggle.style.animation = 'none';     
      }, 1000);
}


/*
* Closes all the menu items (#menu -> ul -> li) all at once by moving them to the top
*/
function closeMenuItems(menu){
      let menuContent = menu.getElementsByTagName('ul')[0];
      
      menuContent.setAttribute('class', 'animate goUpExit');

      setTimeout(function(){
            menuContent.setAttribute('class', '');
      }, 500);
}

/*
* Opens the menu items one by one
*/
function openMenuItems(menu){
      let menuContent = menu.getElementsByTagName('ul')[0];
      let menuItems = menuContent.getElementsByTagName('li');

      for(let i = 0; i < menuItems.length; i++){
            menuItems[i].style.opacity = "0";
      }

      for(let i = 0; i < menuItems.length; i++){
            menuItems[i].style.animation = "comeInLeftEntrance ease-in-out .5s";
            menuItems[i].style.animationDelay = i * 0.10 + "s";

            setTimeout(function(){
                  menuItems[i].style.opacity = "1";
            }, 500);
      }
}

function imageCaroussel(images, element, timeout){
      let i = 0;
      setInterval(function(){
            updateCarousselImage(images[i], element);
            i++;
            if(i == images.length){
                  i = 0;
            }
      }, timeout);
}

function updateCarousselImage(images, element){
      let originalClass = element.getAttribute('class');
      element.setAttribute('class', originalClass + ' animate fadeOut');
      setTimeout(function(){
            element.style.backgroundImage = "url(" + images + ")";
            element.setAttribute('class', originalClass + " animate fadeIn");
      }, 500);

      setTimeout(function(){
            element.setAttribute('class', originalClass);
      }, 1000);
}