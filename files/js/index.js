let loadedProjects = false;
let loadedStats = false;

/*
*     Makes the triangle for the homepage header
*/
function homepageTriangle(){
      setTriangle(
            document.getElementsByClassName('triangle')[0],
            "100%",
            "150px",
            "white"
      );
}

function showProjects(){
      loadIntoElement(document.getElementById('project_container'), function(){
            return new Promise(function(resolve, reject){
                  openJSON("files/js/projects.json").then(function(json){
                        resolve(projectHTML(json));
                        });         
                  });
            });      
         
}

function projectHTML(json){
            let html = "";

            for(let project of json.school){
                  html += `<div class="project"><div class="content">`;
                  let images = project.showcase_images;
                  images.push(project.main_image);

                  images = images.join();
                  html += `<div class="background" data-images="`+ images +`" style="background-image: url(`+ project.main_image +`)" ></div>`;
                  html += `<h3>` + project.name + `</h3>`;
                  html += `<p>` + project.description_short + `</p>`;
                  html += `<a href="`+ project.website_page +`">More information</a>`;
                  if(project.gitlab_url != null){
                        html += `<a href="`+ project.gitlab_url +`">View on GitLab</a>`
                  }
                  html += `</div></div>`;
            }

            return html;
}

function startProjectCaroussel(images, element){
      imageCaroussel(images, element, 5000);
}

function loadStats(){
      let table = document.getElementById('statsTable');
      let elems = table.getElementsByTagName('tr');

      for(let i = 0; i < elems.length; i++){
            let tr = elems[i]
                  .getElementsByTagName('td')[1]
                  .getElementsByTagName('div')[0];
            
            tr.style.transformOrigin = "left";
            tr.style.animation = "progressLine .5s ease-in-out";
            tr.style.animationDelay = (i * 0.10) + "s";
            tr.style.animationFillMode = "forwards";
      }
}

window.onload = function () {
      homepageTriangle();
}

window.onresize = function(){
      homepageTriangle();
}

window.onscroll = function(){

      if(!loadedProjects){
            if(isVisible(document.getElementById('projects'))){
                  this.showProjects();
                  loadedProjects = true;
            }
      }   

      if(!loadedStats){
            if(isVisible(document.getElementById('stats'))){
                  loadStats();
                  loadedStats = true;
            }
      }


      isVisibleMultiple("project", function(e){
            let c = e.getAttribute('class');
            
            e.setAttribute('class', c + ' animate turnIn');
            setTimeout(function(){
                  e.style.opacity = "1";
            }, 1000);
      });
}

document.getElementById('projects').addEventListener('DOMNodeInserted', function(e){
      let projectBackground = e.target.getElementsByClassName('background')[0];
      let images = projectBackground.getAttribute('data-images');
      startProjectCaroussel(images.split(","), projectBackground);
});