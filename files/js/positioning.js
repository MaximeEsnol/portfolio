
/*
*     Makes a triangle 
*/
function setTriangle(element, width, height, color){
      let bLeft, bRight, bTop;

      element.style.width = "0";
      element.style.height = "0";
      element.style.marginBottom = "20px";

      if(width.includes("px")){
            bLeft = element.style.borderLeft = "solid transparent " + width;
            bRight = element.style.borderRight = "solid transparent " + width;
      } else if(width.includes("%")){
            bLeft = element.style.borderLeft = "solid transparent " + (element.parentElement.offsetWidth * (parseInt(width)/100))/2 + "px"; 
            bRight = element.style.borderRight = "solid transparent " + (element.parentElement.offsetWidth * (parseInt(width)/100))/2 + "px"; 
      }

      if(height.includes("px")){
            bTop = element.style.borderBottom = "solid " + color + " " + height;
      } else if(height.includes("%")){
            bTop = element.style.borderBottom = "solid " + color + " " + element.parentElement.offsetHeight * (parseInt(height)/100) + "px";
      }
}

function isVisible(element){
      let pos = element.getBoundingClientRect().top;
      return (pos <= window.innerHeight && pos >= 0);
}

function isVisibleMultiple(elementClass, callback){
      let elements = document.getElementsByClassName(elementClass);

      for(let i = 0; i < elements.length; i++){
            if(isVisible(elements[i])){
                  callback(elements[i]);
                  break;
            }
      }
}